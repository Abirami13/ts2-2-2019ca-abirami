package bus;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import crudoperation.MainFrame;

public class AddBusDetails {
	public AddBusDetails(){
		JFrame frame=new JFrame("Add Bus Details");
		 frame.setSize(400,500);
		 
		 JLabel busname=new JLabel("BUS NAME");
		 busname.setBounds(23,50,100,30);
		 frame.add(busname);
		  
		 JTextField nameField=new JTextField();
		 nameField.setBounds(150,50,100,30);
		 frame.add(nameField);
		 
		 JLabel regnum=new JLabel("BUS NUMBER");
		 regnum.setBounds(23,100,100,30);
		 frame.add(regnum);
		 
		 JTextField bus_textField=new JTextField();
		 bus_textField.setBounds(150,100,100,30);
		 frame.add(bus_textField);
		  
		 JLabel startpoint=new JLabel("SOURCE");
		 startpoint.setBounds(23,150,100,30);
		 frame.add(startpoint);
		 
		 JTextField city_nameField=new JTextField();
		 city_nameField.setBounds(150,150,100,30);
		 frame.add(city_nameField);
		 
		 JLabel starttime=new JLabel("START TIME");
		 starttime.setBounds(23,190,100,30);
		 frame.add(starttime);
		 
		 JTextField time_idField=new JTextField();
		 time_idField.setBounds(150,190,100,30);
		 frame.add(time_idField);
		 
		 JLabel endpoint=new JLabel("DESTINATION");
		 endpoint.setBounds(23,240,100,30);
		 frame.add(endpoint);
		 
		 JTextField city_nameField2=new JTextField();
		 city_nameField2.setBounds(150,240,100,30);
		 frame.add(city_nameField2);
		
		 JLabel droptime=new JLabel("DROP TIME");
		 droptime.setBounds(23,290,100,30);
		 frame.add(droptime);
		 
		 JTextField time_idField2=new JTextField();
		 time_idField2.setBounds(150,290,100,30);
		 frame.add(time_idField2);
		 
		 JButton add=new JButton("ADD");
		 add.setBounds(30,340,100,30);
		 
		 	 frame.add(add);
		
		  add.addActionListener(new ActionListener()
		 {  
	     public void actionPerformed(ActionEvent e)
	     { 
	    	 
			try
			{  
				     Class.forName("oracle.jdbc.driver.OracleDriver");  
					   
					 Connection con=DriverManager.getConnection
					 ("jdbc:oracle:thin:@localhost:1521:xe","system","kalajayapal");  
					   
					 PreparedStatement stmt=con.prepareStatement("insert into busdetails values(?,?,?,?,?,?)");  
					 
					 stmt.setString(1,nameField.getText());
					 
					 int regnum=Integer.parseInt(bus_textField.getText());
					 stmt.setInt(2,regnum); 
					 
					 
					 stmt.setString(3,city_nameField.getText()); 
					 
					 int time1=Integer.parseInt(time_idField.getText());
					 stmt.setInt(4,time1);
						
					 stmt.setString(5,city_nameField2.getText());
					 
					 int time2=Integer.parseInt(time_idField2.getText());
				     stmt.setInt(6,time2);
				     
					 int i=stmt.executeUpdate();  
					 JOptionPane.showMessageDialog(frame,i+"record inserted"); 
					 System.out.println(i+" record inserted");    
					 con.close();  
					   
			} 
			catch(Exception e1)
			{ 
		      System.out.println(e1);  
					   
			}}      
		     });
		  JButton goBack=new JButton("BACK");
			goBack.setBounds(190,340,100,30);
			goBack.addActionListener(new ActionListener()
			{  
				 public void actionPerformed(ActionEvent e)
				 {  
					 AdminHomePage goback1=new AdminHomePage();
					 frame.dispose();       
			     }  
			});  
			 frame.add(goBack);
		 	
		  
		  

			 frame.setLayout(null);
			 frame.setVisible(true);
}
	
}